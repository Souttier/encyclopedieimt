
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../headers/Encyclopedie.h"
#include "../headers/LiseurFichier.h"

char *readUntil(char cStop, FILE *openedFile, char *returnValue)
{
    if (openedFile == NULL || (feof(openedFile)))
        return returnValue;

    char *currentString = (char *)malloc(sizeof(char));
    char c = fgetc(openedFile);
    int i_Char = 0;
    while (c != cStop && !(feof(openedFile)))
    {
        currentString = (char *)realloc(currentString, sizeof(char) * (i_Char + 1));
        currentString[i_Char] = c;
        c = fgetc(openedFile);
        i_Char++;
    }

    if (feof(openedFile))
    {
        if (currentString!=NULL)free(currentString);
        return returnValue;
    }

    currentString = (char *) realloc(currentString, sizeof(char)* (i_Char+1));
    currentString[i_Char] = '\0';
    return currentString;
}

Encyclopedie fillEncyclopedie(Encyclopedie e, FILE* file)
{
    if (file != NULL)
    {
        int id = 0;
        char *texte = readUntil('|', file, NULL);
        char *titre;
        char *description;

        while (texte!=NULL)
        {
            id = atoi(texte);
            free(texte);

            texte = readUntil('|', file, NULL);
            titre = texte;

            texte = readUntil('\n', file, NULL);
            description = texte;

            e = inserer(e, id, titre, description);
            texte = readUntil('|', file, NULL);
        }
        free(texte);
    }
    else
    {
        printf("Impossible d'ouvrir le fichier.\n");
    }
    return e;
}

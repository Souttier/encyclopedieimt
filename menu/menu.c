#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#include "../headers/Encyclopedie.h"
#include "../headers/LiseurFichier.h"

Encyclopedie e;

/** Prototypes "privés" **/

int getMenuChoice(int inf, int sup);
int isNumber(char *str);
void execChoice(int choice);
void displayCellWithString(char *str, int i, int j);
void clearConsole();
void insertArticle();
void deleteArticle();
void findArticlesByWord();
void flushField(char *field);

Article *rechercher_article_entier(Encyclopedie e, int i);

/** Fonctions "privées" **/

/* METHODES POUR L'ENCYCLOPEDIE */

Article *rechercher_article_entier(Encyclopedie e, int i)
{
    Article *result = rechercher_article_id(e, i);
    if (result != NULL)
    {
        return result;
    }
    displayCellWithString("L'article n'existe pas.", 15, 14);
    return NULL;
}

/* METHODES D'AFFICHAGE POUR LES MENUS */

void displayCellWithString(char *msg, int nbOfSpacesBefore, int nbOfSpacesAfter)
{
    printf(" ____________________________________________________\n");
    printf("|                                                    |\n");
    printf("|");
    for (int i = 0; i < nbOfSpacesBefore; i++)
    {
        printf(" ");
    }
    printf("%s", msg);
    for (int j = 0; j < nbOfSpacesAfter; j++)
    {
        printf(" ");
    }
    printf("|\n");
    printf("|____________________________________________________|\n");
}

void displayStartMenu()
{
    #ifdef ENC_LIST
    displayCellWithString("Encyclopédie Liste - Nathan P. & Samuel O.", 5, 5);
    #endif  
    #ifdef ENC_ABR
    displayCellWithString("Encyclopédie ABR - Nathan P. & Samuel O.", 6, 6);
    #endif
    #ifdef ENC_HASH
    displayCellWithString("Encyclopédie Hash - Nathan P. & Samuel O.", 5, 6);
    #endif

    displayCellWithString("1. Insérer un article", 16, 15);
    displayCellWithString("2. Rechercher un article", 14, 14);
    displayCellWithString("3. Rechercher plusieurs articles", 10, 10);
    displayCellWithString("4. Supprimer un article", 15, 14);
    displayCellWithString("5. Afficher l'encyclopédie", 13, 13);
    displayCellWithString("6. Quitter", 21, 21);
    int startChoice = getMenuChoice(1, 6);
    execChoice(startChoice);
}

void displayField(char *label, char *field)
{
    printf("%s : %s\n", label, field);
}

/* ACTIONS DANS LE MENU */

int getMenuChoice(int inf, int sup)
{
    char *input = malloc(sizeof(char));
    printf("\nFaites votre choix: ");
    scanf("%s", input);
    char choice = input[0];
    int intChoice = atoi(&choice);
    while (!isdigit(choice) || !(intChoice >= inf && intChoice <= sup))
    {
        clearConsole();
        displayStartMenu();
        printf("\nChoix impossible.");
        printf("\nFaites votre choix :");
        scanf("%s", input);
        choice = input[0];
        intChoice = atoi(&choice);
    }
    return intChoice;
}

char *askForField(char *msg, int size, int checkNumber, int first)
{
    printf("%s", msg);
    char *field = (char *)malloc(sizeof(char) * size);
    if (first == 1)
    {
        fgets(field, size, stdin);
    }
    if (checkNumber == 1)
    {
        char *temp = fgets(field, size, stdin);
        while (isNumber(temp) == 0)
        {
            printf("Vous devez entrer un nombre.\n");
            printf("%s", msg);
            temp = fgets(field, size, stdin);
        }
    } else {
        fgets(field, size, stdin);
    }
    return field;
}

void insertArticle()
{
    clearConsole();
    displayCellWithString("Insérer un article", 17, 17);
    char *id = askForField("ID : ", 50, 1, 1);
    flushField(id);
    char *titre = askForField("TITRE : ", 255, 0, 0);
    flushField(titre);
    char *contenu = askForField("CONTENU : ", 1024, 0, 0);
    flushField(contenu);
    printf("\nConfirmez-vous l'insertion de l'article suivant ?\n");
    displayField("ID", id);
    displayField("TITRE", titre);
    displayField("CONTENU", contenu);
    printf("\n1. OUI\t2. NON");
    int choice = getMenuChoice(1, 2);
    if (choice == 1)
    {
        e = inserer(e, atoi(id), titre, contenu);
        displayCellWithString("Article inséré", 19, 19);
        printf("\n1. OK\n");
        getMenuChoice(1, 1);
        clearConsole();
        displayStartMenu();
    }
    else
    {
        clearConsole();
        displayStartMenu();
    }
}

void searchArticle()
{
    clearConsole();
    displayCellWithString("Rechercher un article", 16, 15);
    printf("\n");
    char *id = askForField("ID de l'article: ", 50, 1, 1);
    Article *result = rechercher_article_entier(e, atoi(id));
    if (result != NULL)
    {
        printf("[ID] %d\n", result->identifiant);
        printf("[TITRE] %s\n", result->titre);
        printf("[CONTENU] %s\n", result->contenu);
    }
    printf("\n1. OK\n");
    getMenuChoice(1, 1);
    clearConsole();
    displayStartMenu();
}

void deleteArticle()
{
    clearConsole();
    displayCellWithString("Supprimer un article", 16, 16);
    char *id = askForField("ID de l'article: ", 50, 1, 1);
    e = supprimer(e, atoi(id));
    displayCellWithString("Article supprimé.", 17, 18);
    printf("1. OK\n");
    getMenuChoice(1, 1);
    clearConsole();
    displayStartMenu();
}

void findArticlesByWord()
{
    clearConsole();
    displayCellWithString("Recherche d'articles par terme", 11, 11);
    char *word = askForField("Mot recherché : ", 255, 0, 1);
    flushField(word);
    Encyclopedie newEncyclopedie = rechercher_article_plein_texte(e, word);

    if (!est_Vide(newEncyclopedie))
    {
        afficher_encyclopedie(newEncyclopedie);
    } else {

        displayCellWithString("Aucun article trouvé.", 15, 15);
    }
    detruire_bibliotheque(newEncyclopedie);
    printf("1. OK\n");
    getMenuChoice(1, 1);
    clearConsole();
    displayStartMenu();
}

void displayEncyclopedie()
{
    clearConsole();
    displayCellWithString("Affichage de l'encyclopédie", 13, 12);
    printf("\n");
    afficher_encyclopedie(e);
    printf("\n1. OK\n");
    getMenuChoice(1, 1);
    clearConsole();
    displayStartMenu();
}

void execChoice(int choice)
{
    switch (choice)
    {
    case 1:
        insertArticle();
        break;
    case 2:
        searchArticle();
        break;
    case 3:
        findArticlesByWord();
        break;
    case 4:
        deleteArticle();
        break;
    case 5:
        displayEncyclopedie();
        break;
    case 6:
        detruire_bibliotheque(e);
        exit(0);
        break;
    }
}

/* FONCTIONS DIVERSES */

int isNumber(char *str)
{
    int check = 1;
    for (int i = 0; i < strlen(str)-1; i++)
    {
        if (isdigit(str[i]) == 0)
        {
            check = 0;
        }
    }
    return check;
}

void flushField(char *field)
{
    field[strlen(field) - 1] = NULL;
}

void clearConsole()
{
    #ifndef _WIN32
    system("clear");
    #else
    system("cls");
    #endif
}

int main(int argc, char **argv)
{
    //INSERTION DU FICHIER
    FILE *fichier = fopen(argv[1], "r");
    e = creer_encyclopedie();
    e = fillEncyclopedie(e,fichier);

    fclose(fichier);
    displayStartMenu();

    return 0;
}

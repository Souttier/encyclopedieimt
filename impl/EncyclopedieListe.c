
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/Encyclopedie.h"
#include "../headers/EncyclopedieListe.h"

#ifdef ENC_LIST

/* PROTOTYPES "Privés" */

Article *position(Article *article_courant);
Article *rechercher_article_id(Encyclopedie e, int i);
Article *rechercher_article_avant(Encyclopedie e, int i);
Encyclopedie encyclopedie_plein_texte(Encyclopedie e, Article *article, char *mot);
void detruire_article(Article *article);

/* Fonctions "publiques" */

Encyclopedie creer_encyclopedie()
{
	Encyclopedie e = {NULL, NULL};
	return e;
}

Encyclopedie inserer(Encyclopedie e, int identifiant, char* titre, char* contenu)
{
	if (e.racine == NULL)
	{
		e.racine = (Article *)malloc(sizeof(Article));
		*e.racine = (Article){identifiant, titre, contenu, NULL};
	}
	else
	{
		Article *dernier = position(e.article_courant);
		dernier->suivant = (Article *)malloc(sizeof(Article));
		*(dernier->suivant) = (Article){identifiant, titre, contenu, NULL};
	}
	e.article_courant = e.racine;
	return e;
}

Article *rechercher_article_id(Encyclopedie e, int i)
{
	if (e.racine != NULL)
	{
		if (e.article_courant->identifiant != i)
		{
			if (e.article_courant->suivant != NULL)
			{
				e.article_courant = e.article_courant->suivant;
				e.article_courant = rechercher_article_id(e, i);
			}
			else
			{
				e.article_courant = e.racine;
				return NULL;
			}
		}

		if (e.article_courant != NULL)
		{
			Article *result = e.article_courant;
			e.article_courant = e.racine;
			return result;
		}
	}
	else
	{
		e.article_courant = e.racine;
		return NULL;
	}

	e.article_courant = e.racine;
	return NULL;
}

void afficher_encyclopedie(Encyclopedie e)
{
	if (e.racine != NULL)
	{
		printf("%d // %s \n"/*// %s\n"*/, e.article_courant->identifiant, e.article_courant->titre/*, e.article_courant->contenu*/);
		if (e.article_courant->suivant != NULL)
		{
			e.article_courant = e.article_courant->suivant;
			afficher_encyclopedie(e);
		}
	}
	else
	{
		printf("Impossible d'afficher une encyclopédie vide.\n");
	}
	e.article_courant = e.racine;
}

char *rechercher_article( Encyclopedie e, int identifiant)
{
	Article *result = rechercher_article_id(e, identifiant);
	if (result != NULL)
	{
		return result->contenu;
	}
	//printf("L'article n°%d n'existe pas.\n", identifiant);
	return NULL;
}

Encyclopedie rechercher_article_plein_texte(Encyclopedie e, char* mot)
{
	Encyclopedie new_encyclopedie = creer_encyclopedie();
	return encyclopedie_plein_texte(new_encyclopedie, e.racine, mot);
}

Encyclopedie supprimer(Encyclopedie e, int identifiant)
{
	Article *toDelete = rechercher_article_id(e, identifiant);
	if (toDelete != NULL)
	{
		if (toDelete == e.racine)
		{
			e.racine = toDelete->suivant;
			free(toDelete);
		}
		else
		{
			Article *before = rechercher_article_avant(e, identifiant);
			if (toDelete->suivant != NULL)
			{
				before->suivant = toDelete->suivant;
			}
			else
			{
				before->suivant = NULL;
			}
			free(toDelete);
		}
	}
	/*else
	{
		printf("\nL'article n°%d n'existe pas.\n", identifiant);
	}*/
	e.article_courant = e.racine;
	return e;
}

int est_Vide(Encyclopedie e)
{
    return (e.racine == NULL);
}

void detruire_bibliotheque(Encyclopedie e)
{
	if (e.racine!=NULL)
	{
		e.article_courant = e.racine;
		detruire_article(e.racine);
	}
}

/* Fonctions "privées" */

Article *position(Article *article_courant)
{
	if (article_courant->suivant != NULL)
	{
		article_courant = position(article_courant->suivant);
	}
	return article_courant;
}

Article *rechercher_article_avant(Encyclopedie e, int i)
{
	if (e.article_courant->suivant->identifiant != i)
	{
		e.article_courant = e.article_courant->suivant;
		e.article_courant = rechercher_article_avant(e, i);
	}

	if (e.article_courant->suivant != NULL)
	{
		Article *result = e.article_courant;
		e.article_courant = e.racine;
		return result;
	}

	e.article_courant = e.racine;
	return NULL;
}

Encyclopedie encyclopedie_plein_texte(Encyclopedie e, Article *article, char *mot)
{
	if (article != NULL)
	{
		if (strstr(article->contenu, mot) != NULL)
		{
			char *nouveau_titre = (char *)malloc(sizeof(char) * (strlen(article->titre) + 1));
			char *nouveau_contenu = (char *)malloc(sizeof(char) * (strlen(article->contenu) + 1));
			strncpy(nouveau_titre, article->titre, strlen(article->titre));
			strncpy(nouveau_contenu, article->contenu, strlen(article->contenu));
			nouveau_titre[strlen(article->titre)] = '\0';
			nouveau_contenu[strlen(article->contenu)] = '\0';
			e = inserer(e,
						article->identifiant,
						nouveau_titre,
						nouveau_contenu);
		} 
		if (article->suivant != NULL)
		{
			e = encyclopedie_plein_texte(e, article->suivant, mot);
		}
	}
	/*if(e.racine == NULL) {
		e = inserer(e, 0, "Aucun résultat", NULL);
	}*/
	return e;
}

void detruire_article(Article *article)
{
	if (article->suivant != NULL)
	{
		detruire_article(article->suivant);
	}
	free(article);
}

#endif // ENC_LIST

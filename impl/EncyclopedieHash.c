
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/Encyclopedie.h"
#include "../headers/EncyclopedieHash.h"

#ifdef ENC_HASH

/* Prototypes "priv�s" */

void supprimer_article(Article* article);

/* Fonctions "publiques" */

Encyclopedie creer_encyclopedie()
{
    Encyclopedie e = (Encyclopedie) {NULL,NULL,20};
    e.liste_article = (Article**)malloc(e.nbListe*sizeof(Article*));
    e.taille_listes = (int*)malloc(e.nbListe*sizeof(int));

    int i_indice_liste=0;
    for (i_indice_liste=0;i_indice_liste<e.nbListe;i_indice_liste++)
        e.taille_listes[i_indice_liste]=0;

    return e;
}

Encyclopedie inserer(Encyclopedie e, int identifiant, char* titre, char* contenu)
{
    if (e.taille_listes[identifiant%e.nbListe]==0)
    {
        e.liste_article[identifiant%e.nbListe] = (Article*)malloc(sizeof(Article));
    }
    else
    {
        e.liste_article[identifiant%e.nbListe] = (Article*)realloc(e.liste_article[identifiant%e.nbListe],sizeof(Article)*(e.taille_listes[identifiant%e.nbListe]+2));
    }

    e.liste_article[identifiant%e.nbListe][e.taille_listes[identifiant%e.nbListe]++] = (Article){identifiant,titre,contenu};

    return e;
}

Encyclopedie supprimer( Encyclopedie e, int identifiant )
{
    if (e.liste_article[identifiant%e.nbListe]==NULL)
    {
        return e;
    }

    short est_trouve=0;
    int i_article=0;
    for (i_article=0; i_article<e.taille_listes[identifiant%e.nbListe];i_article++)
    {
        if (e.liste_article[identifiant%e.nbListe][i_article].identifiant==identifiant)
        {
            //supprimer_article(&(e.liste_article[identifiant%e.nbListe][i_article]));
            est_trouve=1;
        }

        if (est_trouve)
        {
            if (i_article==e.taille_listes[identifiant%e.nbListe]-1)
            {
                //supprimer_article(&(e.liste_article[identifiant%e.nbListe][i_article+1]));
                break;
            }
                
            e.liste_article[identifiant%e.nbListe][i_article] = e.liste_article[identifiant%e.nbListe][i_article+1];
        }
    }

    if (est_trouve)
    {
        e.liste_article[identifiant%e.nbListe]=(Article*)realloc(e.liste_article[identifiant%e.nbListe],sizeof(Article)*(e.taille_listes[identifiant%e.nbListe]--));
        //if (e.taille_listes[identifiant%e.nbListe]==0)
        //{
            //free(e.taille_listes/*[identifiant%e.nbListe]*/);
            //e.taille_listes[identifiant%e.nbListe] = NULL;
        //}
    }
    //else
    //    printf("\nPas d'article avec l'id %d",identifiant);

    return e;
}

char* rechercher_article( Encyclopedie e, int identifiant)
{
    char* titre_trouve=NULL;
    /*if (e.liste_article[identifiant%e.nbListe]==NULL)
    {
        printf("\nPas d'article avec l'id %d",identifiant);
    }
    else*/
    if (e.liste_article[identifiant%e.nbListe]!=NULL)
    {
        int i_article=0;
        for (i_article=0; i_article<e.taille_listes[identifiant%e.nbListe];i_article++)
        {
            if (e.liste_article[identifiant%e.nbListe][i_article].identifiant==identifiant)
                titre_trouve = e.liste_article[identifiant%e.nbListe][i_article].titre;
        }
    }

    return titre_trouve;
}

Article *rechercher_article_id(Encyclopedie e, int i)
{
    Article* article_trouve=NULL;
    /*if (e.liste_article[i%e.nbListe]==NULL)
    {
        printf("\nPas d'article avec l'id %d",i);
    }
    else*/
    if (e.liste_article[i%e.nbListe]!=NULL)
    {
        int i_article=0;
        for (i_article=0; i_article<e.taille_listes[i%e.nbListe];i_article++)
        {
            if (e.liste_article[i%e.nbListe][i_article].identifiant==i)
                article_trouve = &(e.liste_article[i%e.nbListe][i_article]);
        }
    }
    return article_trouve;
}

Encyclopedie rechercher_article_plein_texte( Encyclopedie e, char* mot)
{
    Encyclopedie nouvelle_encyclopedie = creer_encyclopedie();

    int i_liste_indice=0;
    for (i_liste_indice=0; i_liste_indice<e.nbListe; i_liste_indice++)
    {
        int i_article=0;
        for (i_article=0; i_article<e.taille_listes[i_liste_indice];i_article++)
        {
            if (strstr(e.liste_article[i_liste_indice][i_article].contenu,mot))
            {
                char* nouveau_titre = (char*)malloc(sizeof(char)*(strlen(e.liste_article[i_liste_indice][i_article].titre)+1));
                char* nouveau_contenu = (char*)malloc(sizeof(char)*(strlen(e.liste_article[i_liste_indice][i_article].contenu)+1));
                strncpy(nouveau_titre,e.liste_article[i_liste_indice][i_article].titre, strlen(e.liste_article[i_liste_indice][i_article].titre));
                strncpy(nouveau_contenu,e.liste_article[i_liste_indice][i_article].contenu, strlen(e.liste_article[i_liste_indice][i_article].contenu));

                nouveau_titre[strlen(e.liste_article[i_liste_indice][i_article].titre)] = '\0';
                nouveau_contenu[strlen(e.liste_article[i_liste_indice][i_article].contenu)] = '\0';

                nouvelle_encyclopedie = inserer(nouvelle_encyclopedie,
                                                e.liste_article[i_liste_indice][i_article].identifiant,
                                                nouveau_titre,
                                                nouveau_contenu);
            }

        }
    }
    return nouvelle_encyclopedie;
}

int est_Vide(Encyclopedie e)
{
    int somme=0;

    int i_indice_liste=0;
    for (i_indice_liste=0;i_indice_liste<e.nbListe;i_indice_liste++)
        somme+=e.taille_listes[i_indice_liste];

    return (somme==0);
}

void detruire_bibliotheque(Encyclopedie e)
{
    free(e.liste_article);

    /*int i_taille=0;
    for (i_taille=0; i_taille<e.nbListe;i_taille++)
    {
        free(e.taille_listes);
    }*/
    free(e.taille_listes);
    e.nbListe=0;
}

void afficher_encyclopedie(Encyclopedie e)
{
    int i_liste_indice=0;

    if (!est_Vide(e))
    {
        for (i_liste_indice=0; i_liste_indice<e.nbListe; i_liste_indice++)
        {
            //printf("\n[%d]\t",i_liste_indice);
            int i_article=0;
            for (i_article=0; i_article<e.taille_listes[i_liste_indice];i_article++)
            {
                printf("%d // %s \n"/*// %s\n"*/, e.liste_article[i_liste_indice][i_article].identifiant, e.liste_article[i_liste_indice][i_article].titre/*, e.liste_article[i_liste_indice][i_article].contenu*/);
                /*
                printf("Article(%d)[%s]",e.liste_article[i_liste_indice][i_article].identifiant,e.liste_article[i_liste_indice][i_article].titre);

                if (i_article<e.taille_listes[i_liste_indice]-1) printf(",");
                */
            }
        }
    }
    else
    {
        printf("Impossible d'afficher une encyclop�die vide.\n");
    }

}

/* Fonctions "priv�es" */

void supprimer_article(Article* article)
{
    if (article!=NULL) {free(article) ;}
}

#endif

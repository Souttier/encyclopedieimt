
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/Encyclopedie.h"
#include "../headers/EncyclopedieABR.h"
#ifdef ENC_ABR

/* Prototypes "priv�s" */

Article* position_ajout(Article* article_courant, int identifiant);
Article* avoir_article(Article* article_courant, int i);
Article* avoir_minimum_depuis(Article* article_depart);
Article* avoir_maximum_depuis(Article* article_depart);
Article* avoir_predecesseur(Article* article_depart, int i);
void detruire_article(Article* article);
Encyclopedie association_article_plein_texte( Encyclopedie e, Article* article, char* mot);
void afficher_articles_depuis(Article* article);

/* Fonctions "publiques" */

Encyclopedie creer_encyclopedie()
{
    Encyclopedie e = {NULL,NULL};
    return e;
}

Encyclopedie inserer( Encyclopedie e, int identifiant, char* titre, char* contenu )
{
    if (e.racine==NULL)
    {
        e.racine = (Article*)malloc(sizeof(Article));
        e.article_courant = (Article*)malloc(sizeof(Article));
        *e.racine = (Article){identifiant,titre,contenu,NULL,NULL};
        e.article_courant = e.racine;
    }
    else
    {
        e.article_courant = e.racine;
        e.article_courant = position_ajout(e.article_courant,identifiant);
        if(identifiant<e.article_courant->identifiant)
        {
            e.article_courant->inferieur = (Article*)malloc(sizeof(Article));
            *e.article_courant->inferieur = (Article){identifiant,titre,contenu,NULL,NULL};
        }
        else
        {
            e.article_courant->superieur = (Article*)malloc(sizeof(Article));
            *e.article_courant->superieur = (Article){identifiant,titre,contenu,NULL,NULL};
        }
    }
    return e;
}

Encyclopedie supprimer(Encyclopedie e, int identifiant)
{
    Article* article_courant = avoir_article(e.racine,identifiant);
    Article* predecesseur = avoir_predecesseur(e.racine,identifiant);

    if (article_courant==NULL) return e;//Article inexistant

    if (article_courant->inferieur==NULL && article_courant->superieur==NULL)
    {
        if (article_courant==predecesseur) //On supprime la racine
        {
            free(article_courant);
            e.racine = NULL;
        }
        else
        {
            if (identifiant<predecesseur->identifiant)
                predecesseur->inferieur=NULL;
            else
                predecesseur->superieur=NULL;
            free(article_courant);
        }


    }
    else if (article_courant->inferieur==NULL && article_courant->superieur!=NULL)
    {   // Cas article courant avec un seul enfant superieur
        if (article_courant==predecesseur) //On supprime la racine
        {
            Article* nouvelleRacine = avoir_minimum_depuis(article_courant->superieur);
            predecesseur = avoir_predecesseur(e.racine,nouvelleRacine->identifiant);
            if (nouvelleRacine->identifiant<predecesseur->identifiant)
                predecesseur->inferieur=NULL;
            else
                predecesseur->superieur=NULL;

            e.racine = nouvelleRacine;
            nouvelleRacine->inferieur = article_courant->inferieur;
            nouvelleRacine->superieur = article_courant->superieur;
            free(article_courant);
        }
        else
        {
            if (identifiant<predecesseur->identifiant)
                predecesseur->inferieur=article_courant->superieur;
            else
                predecesseur->superieur=article_courant->superieur;

            article_courant->superieur=NULL;
            free(article_courant);
        }
    }
    else if (article_courant->superieur==NULL && article_courant->inferieur!=NULL)
    {
        if (article_courant==predecesseur) //On supprime la racine
        {
            Article* nouvelleRacine = avoir_maximum_depuis(article_courant->inferieur);
            predecesseur = avoir_predecesseur(e.racine,nouvelleRacine->identifiant);
            if (nouvelleRacine->identifiant<predecesseur->identifiant)
                predecesseur->inferieur=NULL;
            else
                predecesseur->superieur=NULL;

            e.racine = nouvelleRacine;
            nouvelleRacine->inferieur = article_courant->inferieur;
            nouvelleRacine->superieur = article_courant->superieur;
            free(article_courant);
        }
        else
        {
            if (identifiant<predecesseur->identifiant)
                predecesseur->inferieur=article_courant->inferieur;
            else
                predecesseur->superieur=article_courant->inferieur;
            article_courant->inferieur=NULL;
            free(article_courant);
        }
    }
    else if (article_courant->inferieur!=NULL && article_courant->superieur!=NULL)
    {
        if (article_courant==predecesseur) //On supprime la racine
        {
            Article* nouvelleRacine = avoir_minimum_depuis(article_courant->superieur);
            predecesseur = avoir_predecesseur(e.racine,nouvelleRacine->identifiant);
            if (nouvelleRacine->identifiant<predecesseur->identifiant)
                predecesseur->inferieur=NULL;
            else
                predecesseur->superieur=NULL;

            e.racine = nouvelleRacine;
            nouvelleRacine->inferieur = article_courant->inferieur;
            nouvelleRacine->superieur = article_courant->superieur;
            free(article_courant);
        }
        else
        {
            Article* minimum = avoir_minimum_depuis(article_courant->superieur);//On r�cup�re la valeur sup�rieure la plus proche de i
            if (identifiant<predecesseur->identifiant)
                predecesseur->inferieur=minimum;
            else
                predecesseur->superieur=minimum;

            //predecesseur devient le predecesseur de minimum
            predecesseur = avoir_predecesseur(article_courant,minimum->identifiant);
            if (predecesseur==article_courant) predecesseur->superieur = NULL;

            if (article_courant->superieur==minimum)
                minimum->superieur=NULL;
            else
                minimum->superieur = article_courant->superieur;
            minimum->inferieur = article_courant->inferieur;

            free(article_courant);
        }
    }

    return e;
} // Fin supprimer

Article *rechercher_article_id(Encyclopedie e, int i)
{
    return avoir_article(e.racine,i);
}

char* rechercher_article( Encyclopedie e, int identifiant )
{
    Article* article_resultant = avoir_article(e.racine,identifiant);
    return article_resultant->contenu;
}

Encyclopedie rechercher_article_plein_texte( Encyclopedie e, char* mot )
{
    e.article_courant = e.racine;
    Encyclopedie nouvelle_encyclopedie = creer_encyclopedie();

    return association_article_plein_texte(nouvelle_encyclopedie, e.article_courant, mot);
}

int est_Vide(Encyclopedie e)
{
    return (e.racine == NULL);
}

void afficher_encyclopedie(Encyclopedie e)
{
    if (!est_Vide(e))
        afficher_articles_depuis(e.racine);
    else
        printf("Impossible d'afficher une encyclop�die vide.\n");
}

void detruire_bibliotheque( Encyclopedie e )
{
    e.article_courant = e.racine;
    detruire_article(e.racine);
}

/* Fonctions "priv�es" */

Article* position_ajout(Article* article_courant, int identifiant)
{
    int position_est_trouve=0;

    if (identifiant < article_courant->identifiant && article_courant->inferieur==NULL)
        position_est_trouve=1;
    if (identifiant >= article_courant->identifiant && article_courant->superieur==NULL)
        position_est_trouve=1;

    if (position_est_trouve==0)
    {
        if (identifiant < article_courant->identifiant)
            article_courant = position_ajout(article_courant->inferieur,identifiant);
        else
            article_courant = position_ajout(article_courant->superieur,identifiant);
    }
    return article_courant;
}

Article* avoir_article(Article* article_courant, int i)
{
    if (article_courant==NULL) return article_courant;

    if (article_courant!=NULL && article_courant->identifiant!=i)
    {
        if (i<article_courant->identifiant)
            article_courant = avoir_article(article_courant->inferieur,i);
        else
            article_courant = avoir_article(article_courant->superieur,i);
    }

    return article_courant;
}

Article* avoir_predecesseur(Article* article_depart, int i)
{
    if (article_depart->identifiant!=i && !(article_depart->inferieur->identifiant==i || article_depart->superieur->identifiant==i))
    {
        if (i<article_depart->identifiant)
            article_depart = article_depart->inferieur;
        else
            article_depart = article_depart->superieur;
    }

    return article_depart;
}

Article* avoir_minimum_depuis(Article* article_depart)
{
    if (article_depart->inferieur!=NULL)
        article_depart= avoir_minimum_depuis(article_depart->inferieur);

    return article_depart;
}

Article* avoir_maximum_depuis(Article* article_depart)
{
    if (article_depart->superieur!=NULL)
        article_depart= avoir_maximum_depuis(article_depart->superieur);

    return article_depart;
}

Encyclopedie association_article_plein_texte( Encyclopedie e, Article* article, char* mot)
{
    if (strstr(article->contenu,mot)!=NULL)
    {
        char* nouveau_titre = (char*)malloc(sizeof(char)*(strlen(article->titre)+1));
        char* nouveau_contenu = (char*)malloc(sizeof(char)*(strlen(article->contenu)+1));
        strncpy(nouveau_titre,article->titre, strlen(article->titre));
        strncpy(nouveau_contenu,article->contenu, strlen(article->contenu));

        nouveau_titre[strlen(article->titre)] = '\0';
        nouveau_contenu[strlen(article->contenu)] = '\0';


        e = inserer(e,article->identifiant,nouveau_titre,nouveau_contenu);
    }


    if (article->inferieur!=NULL)
        e = association_article_plein_texte(e,article->inferieur,mot);
    if (article->superieur!=NULL)
        e = association_article_plein_texte(e,article->superieur,mot);

    return e;
}

void afficher_articles_depuis(Article* article)
{
    if (article!=NULL)
    {
        printf("%d // %s \n"/*// %s\n"*/, article->identifiant, article->titre/*, article->contenu*/);
        afficher_articles_depuis(article->inferieur);
        afficher_articles_depuis(article->superieur);
    }
}

void detruire_article(Article* article)
{
    if (article!=NULL)
    {
        detruire_article(article->inferieur);
        detruire_article(article->superieur);

        free(article);
    }

}

#endif

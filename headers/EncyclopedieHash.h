#ifndef ENCYCLOPEDIEHASH_H_INCLUDED
#define ENCYCLOPEDIEHASH_H_INCLUDED
#include "Encyclopedie.h"

#ifdef ENC_HASH
struct Article
{
    int identifiant;
    char* titre;
    char* contenu;
};

struct Encyclopedie
{
    Article** liste_article;
    int* taille_listes;
    int nbListe;
};
#endif // ENC_HASH

#endif // ENCYCLOPEDIEHASH_H_INCLUDED

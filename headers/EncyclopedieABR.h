#ifndef ENCYCLOPEDIEABR_H_INCLUDED
#define ENCYCLOPEDIEABR_H_INCLUDED
#include "Encyclopedie.h"

#ifdef ENC_ABR
struct Article
{
    int identifiant;
    char* titre;
    char* contenu;
    Article* inferieur;
    Article* superieur;
};

struct Encyclopedie
{
    Article* racine;
    Article* article_courant;
};

#endif // ENC_ABR

#endif // ENCYCLOPEDIEABR_H_INCLUDED

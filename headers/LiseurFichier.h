#ifndef LISEURFICHIER_H_INCLUDED
#define LISEURFICHIER_H_INCLUDED

#include "./Encyclopedie.h"

char *readUntil(char cStop, FILE *openedFile, char *returnValue);
Encyclopedie fillEncyclopedie(Encyclopedie e, FILE* file);


#endif // LISEURFICHIER_H_INCLUDED

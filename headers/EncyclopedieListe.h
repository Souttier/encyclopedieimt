#ifndef ENCYCLOPEDIELISTE_H_INCLUDED
#define ENCYCLOPEDIELISTE_H_INCLUDED
#include "Encyclopedie.h"

#ifdef ENC_LIST
struct Article
{
	int identifiant;
	char *titre;
	char *contenu;
	Article *suivant;
};

struct Encyclopedie
{
	Article *racine;
	Article *article_courant;
};
#endif // ENC_LIST

#endif // ENCYCLOPEDIELISTE_H_INCLUDED

#ifndef ENCYCLOPEDIE_H_INCLUDED
#define ENCYCLOPEDIE_H_INCLUDED

#define ENC_ABR
//#define ENC_LIST
//#define ENC_HASH

typedef struct Article Article;
struct Article;

typedef struct Encyclopedie Encyclopedie;
struct Encyclopedie;

#ifdef ENC_HASH
#include "./EncyclopedieHash.h"
#endif
#ifdef ENC_LIST
#include "./EncyclopedieListe.h"
#endif
#ifdef ENC_ABR
#include "./EncyclopedieABR.h"
#endif


Encyclopedie creer_encyclopedie();
Encyclopedie inserer(Encyclopedie e, int identifiant, char* titre, char* contenu);
Encyclopedie supprimer(Encyclopedie e, int identifiant);
char* rechercher_article( Encyclopedie e, int identifiant);
Encyclopedie rechercher_article_plein_texte( Encyclopedie e, char* mot);
void detruire_bibliotheque(Encyclopedie e);
Article *rechercher_article_id(Encyclopedie e, int i);
void afficher_encyclopedie(Encyclopedie e);
int est_Vide(Encyclopedie e);

#endif // ENCYCLOPEDIE_H_INCLUDED
